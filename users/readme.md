blueprint# PURISTA blueprint


You can add a new service, command or subscription to your project, by simply using following command:  
```bash
purista add [service|command|subscription]
```

Visit [https://purista.dev](https://purista.dev) for documentation