import { serve } from '@hono/node-server'
import { serveStatic } from '@hono/node-server/serve-static'
import { swaggerUI } from '@hono/swagger-ui'
import { gracefulShutdown, initLogger, type Service } from '@purista/core'
import { DefaultEventBridge } from '@purista/core'
import { honoV1Service } from '@purista/hono-http-server'
import { userV1ServiceBuilder } from "./service/user/v1"

export const main = async () => {
  const logger = initLogger()

  const services: Service[] = []

  // initialize the event bridge as first step
  const eventBridge = new DefaultEventBridge({ logger })
  await eventBridge.start()

  // initialize the webserver service as second step
  const port = 3000
  const honoService = await honoV1Service.getInstance(eventBridge, {
    logger,
    serviceConfig: {
      services,
      // this actually instructs the purista-tailored hono to utilize the 'pattern' router https://hono.dev/concepts/routers#patternrouter
      // docs are sparse, but those general examples out from the unit tests should suffice:
      // https://github.com/honojs/hono/blob/34fb36e960a0b48369c9f6047bc57caec7b0ea4b/src/router/common.case.test.ts
      enableDynamicRoutes: true,
    },
  })
  honoService.app.get('/api', swaggerUI({ url: '/api/openapi.json' }))
  honoService.app.get('*', serveStatic({ root: './public' }))
  honoService.openApi.addServer({
    url: `http://localhost:${port}`,
    description: 'PURISTA backend api - OpenApi definition for server endpoints',
  })

  // start the webserver
  await honoService.start()

  const serviceInstance = serve({ fetch: honoService.app.fetch, port })

  // add initiation and start of services here
  const userService = await userV1ServiceBuilder.getInstance(eventBridge);
  await userService.start();
  services.push(userService);

  // try to shut down as clean as possible
  gracefulShutdown(logger, [
    honoService.prepareDestroy(),
    eventBridge,
    ...services,
    {
      name: `${honoService.serviceInfo.serviceName} ${honoService.serviceInfo.serviceVersion} close socket`,
      destroy: async () => {
        serviceInstance.close()
      },
    },
    honoService,
  ])
}

main()
