export enum ServiceEvent {
  /**
   * Emitted by user v1 command signUp:
   * Registers a new user at our product.
   */
  NewUserRegistered = 'newUserRegistered',
  WelcomeEmailSent = 'send a welcome mail to new registered users'
}
