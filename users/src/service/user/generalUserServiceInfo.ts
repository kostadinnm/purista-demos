import type { ServiceInfoType } from '@purista/core'

export const generalUserServiceInfo = {
  serviceName: 'User',
  serviceDescription: 'Manages data related to users.',
} as const satisfies Omit<ServiceInfoType, 'serviceVersion'>
