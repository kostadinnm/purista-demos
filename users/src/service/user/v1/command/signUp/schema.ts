import { extendApi } from '@purista/core'
import { z } from 'zod'

// define the input parameters
export const userV1SignUpInputParameterSchema = extendApi(z.object({}), {title: 'sign up input parameter schema'});

// define the input payload
export const userV1SignUpInputPayloadSchema = extendApi(
    z.object({
        email: extendApi(z.string().email(), {title: 'the email of the user to register', example: 'user@email.com'}),
        name: extendApi(z.string().min(3), {title: 'the name of the user to register', example: 'User'}),
        password: extendApi(z.string().min(3), {title: 'the login password', example: 'password'}),
    }), {title: 'sign up input'});

// define the output payload
export const userV1SignUpOutputPayloadSchema = extendApi(
    z.object({
        userId: extendApi(z.string().uuid(), {title: 'the user id', example: '8d6f7d95-d34c-4fbb-b6e8-203470b6c6ee'}),
    }), {title: 'sign up output'});
