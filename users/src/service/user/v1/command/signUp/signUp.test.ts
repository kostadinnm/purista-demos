import { describe, expect, beforeEach, afterEach, test } from 'vitest'
import { getEventBridgeMock, getLoggerMock, safeBind } from '@purista/core'
import { createSandbox } from 'sinon'

import { userV1Service } from '../../userV1Service.js'
import { signUpCommandBuilder } from './signUpCommandBuilder.js'
import type { UserV1SignUpInputParameter, UserV1SignUpInputPayload } from './types.js'
import { StateStoreKey } from "../../../../../types";

describe('service User version 1 - command signUp', () => {
  let sandbox = createSandbox();
  beforeEach(() => {
    sandbox = createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  test('does not throw', async () => {
    const service = userV1Service.getInstance(getEventBridgeMock(sandbox).mock, { logger: getLoggerMock(sandbox).mock });
    // @ts-ignore
    const signUp = safeBind(signUpCommandBuilder.getCommandFunction(), service);
    const payload: UserV1SignUpInputPayload = undefined;
    const parameter: UserV1SignUpInputParameter = {};
    const context = signUpCommandBuilder.getCommandContextMock(payload, parameter, sandbox);
    const result = await signUp(context.mock, payload, parameter);
    expect(result).toBeUndefined();
  });

  test('can register a new user', async () => {
    const service = userV1Service.getInstance(getEventBridgeMock(sandbox).mock, { logger: getLoggerMock(sandbox).mock });
    // @ts-ignore
    const signUp = safeBind(signUpCommandBuilder.getCommandFunction(), service);
    const payload: UserV1SignUpInputPayload = {
      name: 'test user',
      email: 'email@example.com',
      password: 'password'
    };
    const parameter: UserV1SignUpInputParameter = {};
    const context = signUpCommandBuilder.getCommandContextMock(payload, parameter, sandbox);
    context.stubs.getState.resolves({});
    context.stubs.setState.resolves();
    const result = await signUp(context.mock, payload, parameter);
    expect(result.userId).toBeDefined();
  });

  test('throws when a user with same email exists', async () =>{
    const service = userV1Service.getInstance(getEventBridgeMock(sandbox).mock, { logger: getLoggerMock(sandbox).mock });
    // @ts-ignore
    const signUp = safeBind(signUpCommandBuilder.getCommandFunction(), service);
    const payload: UserV1SignUpInputPayload = {
      name: 'test user',
      email: 'email@example.com',
      password: 'password'
    };
    const parameter: UserV1SignUpInputParameter = {};
    const context = signUpCommandBuilder.getCommandContextMock(payload, parameter, sandbox);
    context.stubs.getState.resolves({
      [StateStoreKey.Users]: [
        {
          name: 'test user',
          email: 'email@example.com',
          password: 'password',
          userId: '35cb7cee-60d5-4dc6-ab2d-cf83b1741477'
        }
      ]
    });
    context.stubs.setState.resolves();
    // const result = await signUp(context.mock, payload, parameter);
    await expect(() => signUp(context.mock, payload, parameter)).rejects.toThrow('the user already exists');
  });
});
