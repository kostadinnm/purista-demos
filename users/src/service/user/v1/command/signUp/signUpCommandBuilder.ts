import { ServiceEvent } from '../../../../ServiceEvent.enum.js';
import { userV1ServiceBuilder } from '../../userV1ServiceBuilder.js';
import {
    userV1SignUpInputParameterSchema,
    userV1SignUpInputPayloadSchema,
    userV1SignUpOutputPayloadSchema,
} from './schema.js';
import { StateStoreKey, User } from "../../../../../types";
import { HandledError, StatusCode } from "@purista/core";

export const signUpCommandBuilder = userV1ServiceBuilder
    .getCommandBuilder('signUp', 'Registers a new user at our product.')
    .setSuccessEventName(ServiceEvent.NewUserRegistered)
    .addPayloadSchema(userV1SignUpInputPayloadSchema)
    .addParameterSchema(userV1SignUpInputParameterSchema)
    .addOutputSchema(userV1SignUpOutputPayloadSchema)
    .setCommandFunction(async function (_context, _payload, _parameter) {
        const result = (await _context.states.getState(StateStoreKey.Users)) as { [StateStoreKey.Users]: User[] | undefined };

        if (result.users?.some(user => user.email === _payload.email)) {
            throw new HandledError(StatusCode.BadRequest, 'the user already exists');
        }

        const user: User = {
            ..._payload,
            userId: crypto.randomUUID()
        };

        const users = result.users ?? [];
        users.push(user);

        await _context.states.setState(StateStoreKey.Users, users);

        _context.logger.info('new user added');

        return user;
    });
