import type { CommandDefinitionList, SubscriptionDefinitionList } from '@purista/core'

import { signUpCommandBuilder } from './command/signUp/signUpCommandBuilder.js'
import { userV1ServiceBuilder } from './userV1ServiceBuilder.js'

// bring service config definition, command definitions and subscription definitions together in the service
// add only definitions and no further service config here
// other service config should be done in ./userServiceBuilder.ts file

const commandDefinitions: CommandDefinitionList<any> = [signUpCommandBuilder.getDefinition()]

const subscriptionDefinitions: SubscriptionDefinitionList<any> = []

export const userV1Service = userV1ServiceBuilder
  .addCommandDefinition(...commandDefinitions)
  .addSubscriptionDefinition(...subscriptionDefinitions)
