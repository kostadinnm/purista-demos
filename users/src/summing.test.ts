import { expect, test } from 'vitest'

import { sum } from './summing'

test('a sample vitest test', () => {
  expect(sum(1, 2)).toBe(3)
})
