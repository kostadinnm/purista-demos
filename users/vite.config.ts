import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    globals: true,
    watch: false,
    environment: 'node',
    coverage: {
      provider: 'istanbul',
      enabled: true,
      include: ['**/src/**/*.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],
    },
  },
})
